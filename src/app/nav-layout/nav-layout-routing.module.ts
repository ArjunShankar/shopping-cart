import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from './components/layout/layout.component';

const childRoutes: Routes = [
  { path: '', component: LayoutComponent,
    children: [

      { path: 'shopping-cart', loadChildren: () => import('../shopping-cart/shopping-cart.module').then(m => m.ShoppingCartModule) },
      { path: 'product/:id', loadChildren: () => import('../product/product.module').then(m => m.ProductModule) },
      { path: 'catalogue', loadChildren: () => import('../catalogue/catalogue.module').then(m => m.CatalogueModule)  },
    //  { path: 'feedback', loadChildren: '../feedback/feedback.module#FeedbackModule' },
      { path: '', redirectTo: 'catalogue', pathMatch: 'full'}
    ]},
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(childRoutes)
  ],
  exports: [RouterModule]
})
export class NavLayoutRoutingModule { }
