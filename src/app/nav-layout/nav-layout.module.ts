import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LayoutComponent } from './components/layout/layout.component';
import {NavLayoutRoutingModule} from './nav-layout-routing.module';
import {MaterialModule} from '../shared/material.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import { CartItemsCountPipe } from './pipes/cart-items-count.pipe';

@NgModule({
  declarations: [NavbarComponent, LayoutComponent, CartItemsCountPipe],
  imports: [
    CommonModule,
    NavLayoutRoutingModule,
    MaterialModule,
    FlexLayoutModule
  ]
})
export class NavLayoutModule { }
