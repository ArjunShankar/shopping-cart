import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Subscription} from 'rxjs';
import {EmitEvent, EventBusService, EventBusTypes} from '../../../core/services/event-bus.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  @Output() public sidenavToggle = new EventEmitter();

  cartCount: number;
  private eventBusSubscription$: Subscription;

  constructor(
    private eventBus: EventBusService,
    private router: Router
  ) {
    this.cartCount = 0;
  }

  ngOnInit() {
    this.eventBusSubscription$ = this.eventBus.on(EventBusTypes.ItemAdded, ((data) => {
      this.cartCount = data.data;
    }));


    //this.eventBus.emit(new EmitEvent(Events.CustomerSelected, {data: 'random'}));
  }

  onToggleSidenav() {
    this.sidenavToggle.emit();
  }

  goToCart() {
    this.router.navigate(['/home/shopping-cart']);
  }

}
