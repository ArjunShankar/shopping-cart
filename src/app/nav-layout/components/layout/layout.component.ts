import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

  public links: Array<any>;

  constructor(
    private router: Router
  ) {
    this.links = [
      {
        route: '/home/catalogue', label: 'Catalogue', icon: 'search'
      },
      // {
      //   route: '/feedback', label: 'Feedback', icon: 'perm_identity'
      // },
      {
        route: '/home/shopping-cart', label: 'Cart', icon: 'shopping_cart'
      }
    ];
  }

  ngOnInit() {

  }

  navClick(evt, link) {
    this.router.navigate([link.route]);
  }


}
