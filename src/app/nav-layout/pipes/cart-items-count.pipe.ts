import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'cartItemsCount'
})
export class CartItemsCountPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let toReturn = '';
    value > 0 ? toReturn = `(${value})` : '' ;
    return toReturn;
  }

}
