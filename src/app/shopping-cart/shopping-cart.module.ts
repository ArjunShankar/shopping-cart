import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShoppingCartComponent } from './components/shopping-cart/shopping-cart.component';
import { CartItemComponent } from './components/cart-item/cart-item.component';
import { SharedModule } from '../shared/shared.module';
import { ShoppingCartRoutingModule } from './shopping-cart-routing.module';

@NgModule({
  declarations: [ ShoppingCartComponent, CartItemComponent ],
  imports: [
    CommonModule,
    SharedModule,
    ShoppingCartRoutingModule
  ]
})
export class ShoppingCartModule { }
