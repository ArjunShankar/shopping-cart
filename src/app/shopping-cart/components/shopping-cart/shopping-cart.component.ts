import { Component, OnInit, OnDestroy } from '@angular/core';
import {CartService} from '../../../core/services/cart.service';
import {CartItem} from '../../../shared/interfaces/interfaces';
import {SubSink} from 'subsink';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.scss']
})
export class ShoppingCartComponent implements OnInit, OnDestroy {

  cartItems: Array<CartItem>;
  private subs = new SubSink();

  constructor(
    private cartService: CartService
  ) { }

  ngOnInit() {
    this.subs.sink = this.cartService.stateChanged.subscribe((value) => { //exposes data as per slice method
      console.log(value);
    });
    /*
    // this will give us the global state
    this.subs.sink = this.cartService.globalStateChanged.subscribe((value) => {
      console.log(value);
    });

     */
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

}
