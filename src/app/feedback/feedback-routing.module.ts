import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {FeedbackFormComponent} from './components/feedback-form/feedback-form.component';


const childRoutes: Routes = [
  { path: '', component: FeedbackFormComponent},
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(childRoutes)
  ],
  exports: [RouterModule]
})
export class FeedbackRoutingModule { }
