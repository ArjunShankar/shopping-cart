import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FeedbackRoutingModule} from './feedback-routing.module';
import { FeedbackFormComponent } from './components/feedback-form/feedback-form.component';
import { ViewModeDirective } from './directives/view-mode.directive';
import { EditModeDirective } from './directives/edit-mode.directive';
import {ReactiveFormsModule} from '@angular/forms';
import { EditableControlComponent } from './components/editable-control/editable-control.component';
import {MaterialModule} from '../shared/material.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  declarations: [FeedbackFormComponent, ViewModeDirective, EditModeDirective, EditableControlComponent],
  imports: [
    CommonModule,
    FeedbackRoutingModule,
    ReactiveFormsModule,
    MaterialModule,
    FlexLayoutModule,
    SharedModule
  ]
})
export class FeedbackModule { }
