import {Component, ContentChild, ElementRef, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {EditModeDirective} from '../../directives/edit-mode.directive';
import {ViewModeDirective} from '../../directives/view-mode.directive';
import {fromEvent, Subject} from 'rxjs';
import { untilDestroyed } from 'ngx-take-until-destroy';
import {filter, switchMapTo, take} from 'rxjs/operators';

@Component({
  selector: 'editable-control',
  templateUrl: './editable-control.component.html',
  styleUrls: ['./editable-control.component.scss']
})
export class EditableControlComponent implements OnInit, OnDestroy {

  @Output() update = new EventEmitter();
  @ContentChild(ViewModeDirective, {static: true}) viewModeTpl: ViewModeDirective;
  @ContentChild(EditModeDirective, {static: true}) editModeTpl: EditModeDirective;

  mode: 'view' | 'edit' = 'view';

  editMode = new Subject();
  editMode$ = this.editMode.asObservable();

  constructor(
    private host: ElementRef
  ) { }

  ngOnInit() {
    this.viewModeHandler();
    this.editModeHandler();
  }

  get currentView() {
    return this.mode === 'view' ? this.viewModeTpl.tpl : this.editModeTpl.tpl;
  }

  private get element() {
    return this.host.nativeElement;
  }

  private viewModeHandler() {
    fromEvent(this.element, 'dblclick').pipe(
      untilDestroyed(this)
    ).subscribe(() => {
      this.mode = 'edit';
      this.editMode.next(true);
    });
  }

  private editModeHandler() {
    const clickOutside$ = fromEvent(document, 'click').pipe(
      filter(({ target }) => this.element.contains(target) === false),
      take(1)
    )

    this.editMode$.pipe(
      switchMapTo(clickOutside$),
      untilDestroyed(this)
    ).subscribe(event => {
      this.update.next();
      this.mode = 'view';
    });
  }

  ngOnDestroy() { }



}
