import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditableControlComponent } from './editable-control.component';

describe('EditableControlComponent', () => {
  let component: EditableControlComponent;
  let fixture: ComponentFixture<EditableControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditableControlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditableControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
