import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {QuicklinkStrategy} from 'ngx-quicklink';

const routes: Routes = [
  { path: 'home', loadChildren: () => import('./nav-layout/nav-layout.module').then(m => m.NavLayoutModule) },
  { path: 'login', loadChildren: () => import('./login/login.module').then(m => m.LoginModule) },
  { path: '', pathMatch: 'full', redirectTo: '/login' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {enableTracing: false, preloadingStrategy: QuicklinkStrategy  })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
