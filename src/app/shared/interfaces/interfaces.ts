export interface Product {
  id: string;
  title: string;
  brand: string;
  price: string;
  description: string;
  image: string;
}

export interface CartItem {
  productId: string;
  count: number;
}

export interface UserInfo {
  username: string;
  preferences: {
    wishlist: Array<number>
  };
}
