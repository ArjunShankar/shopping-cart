import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Product} from '../../../shared/interfaces/interfaces';
import {ProductService} from '../../../core/services/product.service';
import {CartService} from '../../../core/services/cart.service';
import {isPackageNameSafeForAnalytics} from '@angular/cli/models/analytics';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {

  dataProvider: Product;
  quantity: number;
  isFav: boolean;
  favColor: string;

  constructor(
    private activatedRoute: ActivatedRoute,
    private productService: ProductService,
    private cartService: CartService
  ) {
    this.quantity = 0;
    this.isFav = false;
    this.favColor = '';
  }

  ngOnInit() {
    const params: any = this.activatedRoute.snapshot.params;
    if (params && params.id) {
      this.productService.getProduct(params.id).subscribe((data) => {
        this.dataProvider = data;
      });
    }
  }

  addToCart() {
    this.cartService.add(this.dataProvider.id, this.quantity);
  }

  toggleFav() {
    if (this.isFav) {
      this.isFav = false;
      this.favColor = '';
    } else {
      this.isFav = true;
      this.favColor = 'warn';
    }
  }

}
