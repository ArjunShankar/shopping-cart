import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductDetailComponent } from './components/product-detail/product-detail.component';
import { ProductRoutingModule } from './product-routing.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MaterialModule} from '../shared/material.module';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [ProductDetailComponent],
  imports: [
    CommonModule,
    ProductRoutingModule,
    FlexLayoutModule,
    MaterialModule,
    FormsModule
  ]
})
export class ProductModule { }
