import { Injectable } from '@angular/core';
import {Product, UserInfo} from '../../shared/interfaces/interfaces';
import {catchError, map} from 'rxjs/operators';
import {ObservableStore} from '@codewithdan/observable-store';
import {GlobalStore} from '../store/global-store';
import {HttpClient} from '@angular/common/http';

export enum ProductsStoreActions {
  GetUserInfo = 'get_userinfo',
  SetUserInfo = 'set_userinfo'
}

@Injectable({
  providedIn: 'root'
})

export class UserService extends ObservableStore<GlobalStore> {

  remoteURL = '/assets/dummyData/user.json';

  constructor(
    private http: HttpClient
  ) {
    super({ trackStateHistory: true, logStateChanges: true });
  }

  loginUser() {
    return this.http.get<UserInfo>(this.remoteURL)
      .pipe(
        map((userinfo: UserInfo) => {
          this.setState({ user: userinfo }, ProductsStoreActions.SetUserInfo);   // set state in store, return at same time
          return userinfo;
        })
      );

  }
}
