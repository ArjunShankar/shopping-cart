import { Injectable } from '@angular/core';
import { ObservableStore } from '@codewithdan/observable-store';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

import {CartItem, Product} from '../../shared/interfaces/interfaces';
import {GlobalStore} from '../store/global-store';


export enum ProductsStoreActions {
  GetProducts = 'get_products',
  GetProduct = 'get_product',
  InitCart = 'init_cart'
}

@Injectable({
  providedIn: 'root'
})
export class ProductService extends ObservableStore<GlobalStore> {

  remoteURL = '/assets/dummyData/products.json';

  // @ts-ignore
  constructor(private http: HttpClient) {
    super( { trackStateHistory: true, logStateChanges: true });
  }

  get() {
    const currentState = this.getState();
    if (currentState && currentState.products) {
      return of(currentState.products);
    } else {
      return this.getProductsFromServer();
    }
  }

  getProduct(id: string) {
    return this.get()
      .pipe(
        map(products => {
          const filteredList = products.filter(item => item.id === id);
          const product = (filteredList && filteredList.length) ? filteredList[0] : null;
          this.setState({ product }, ProductsStoreActions.GetProduct);
          return product;
        })
      );
  }

  private getProductsFromServer() {
    return this.http.get<Product[]>(this.remoteURL)
      .pipe(
        map(products => {
          this.setState({ products }, ProductsStoreActions.GetProducts);   // set state in store, return at same time
          this.setState({ cartItems: [] }, ProductsStoreActions.InitCart);
          return products;
        }),
        catchError(this.handleError)
      );
  }

  private handleError(error: any) {
    if (error.error instanceof Error) {
      const errMessage = error.error.message;
      return Observable.throw(errMessage);
    }
    return Observable.throw(error || 'Server error');
  }


}
