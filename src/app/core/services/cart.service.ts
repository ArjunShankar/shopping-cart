import { Injectable } from '@angular/core';
import { ObservableStore } from '@codewithdan/observable-store';
import { Observable, of } from 'rxjs';
import {CartItem, Product} from '../../shared/interfaces/interfaces';
import {GlobalStore} from '../store/global-store';
import {EmitEvent, EventBusService, EventBusTypes} from './event-bus.service';

export enum CartStoreActions {
  GetCartItems = 'get_cart_items',
  AddItem = 'add_cart_item',
  RemoveItem = 'remove_cart_item',
  InitializeCart = 'initialize_cart'
}

@Injectable({
  providedIn: 'root'
})
export class CartService extends ObservableStore<GlobalStore> {

  constructor(
    private eventBus: EventBusService
  ) {
    super( { trackStateHistory: true, logStateChanges: true, stateSliceSelector: state => {
        return {
          cartItems: state.cartItems
        };
      }
    });
  }

  get() {
    const currentState = this.getState();
    if (currentState && currentState.cartItems) {
      return of(currentState.cartItems);
    }
  }

  //todo - call remove if 0 items
  add(productId: string, quantity: number) {
    const state = this.getState();
    const itemInCart = this.alreadyExistsInCart(productId, state.cartItems);
    if (itemInCart != null) {
      itemInCart.count = quantity;
    } else {
      const item: CartItem = {
        productId,
        count: quantity
      }
      state.cartItems.push(item);
    }
    this.setState({ cartItems: state.cartItems },  'add_cart_item');
    this.manageTotalItems();
  }
/*
  remove() {
    // Get state from store
    let state = this.getState();
    state.customers.splice(state.customers.length - 1, 1);
    // Set state in store
    this.setState({ customers: state.customers }, 'remove_customer');
  }
*/

  private manageTotalItems() {
    const state = this.getState();
    let totalItems = 0;
    if (state.cartItems && state.cartItems.length > 0) {
      state.cartItems.map((item) => {
        totalItems += item.count;
      });
    }
    this.eventBus.emit(new EmitEvent(EventBusTypes.ItemAdded, {data: totalItems}));
  }

  private alreadyExistsInCart(productId: string, list: CartItem[]) {
    let toReturn: CartItem;
    for (const item of list) {
      if (item.productId === productId) {
        toReturn = item;
        break;
      }
    }

    return toReturn;
  }

  private handleError(error: any) {
    if (error.error instanceof Error) {
      const errMessage = error.error.message;
      return Observable.throw(errMessage);
    }
    return Observable.throw(error || 'Server error');
  }


}
