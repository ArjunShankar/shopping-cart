import {CartItem, Product, UserInfo} from '../../shared/interfaces/interfaces';

export interface GlobalStore {
  cartItems: CartItem[];
  products: Product[];
  product: Product;
  user: UserInfo;
}
