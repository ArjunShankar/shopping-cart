import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

import {UserService} from '../../../core/services/user.service';

import {UserInfo} from '../../../shared/interfaces/interfaces';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(
    private router: Router,
    private userService: UserService
  ) { }

  ngOnInit() {
  }

  login() {
    this.userService.loginUser().subscribe((value) => {
      this.router.navigate(['/home']);
    });
  }

}
