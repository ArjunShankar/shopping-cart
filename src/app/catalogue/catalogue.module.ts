import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CatalogueComponent } from './components/catalogue/catalogue.component';
import {CatalogueRoutingModule} from './catalogue-routing.module';
import { ProductCardComponent } from './components/product-card/product-card.component';
import {MaterialModule} from '../shared/material.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import { MatGridListModule, MatCardModule, MatMenuModule, MatIconModule, MatButtonModule } from '@angular/material';
import { LayoutModule } from '@angular/cdk/layout';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  declarations: [CatalogueComponent, ProductCardComponent],
  imports: [
    CommonModule,
    CatalogueRoutingModule,
    MaterialModule,
    FlexLayoutModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    LayoutModule,
    SharedModule
  ]
})
export class CatalogueModule { }
