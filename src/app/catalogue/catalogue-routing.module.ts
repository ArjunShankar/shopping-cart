import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {CatalogueComponent} from './components/catalogue/catalogue.component';


const childRoutes: Routes = [
  { path: '', component: CatalogueComponent},
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(childRoutes)
  ],
  exports: [RouterModule]
})
export class CatalogueRoutingModule { }
