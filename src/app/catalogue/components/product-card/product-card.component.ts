import {Component, OnInit, Input, EventEmitter, Output} from '@angular/core';
import {Product} from '../../../shared/interfaces/interfaces';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss']
})
export class ProductCardComponent implements OnInit {

  @Input() product: Product;
  @Output() productClick: EventEmitter<any>;

  constructor() {
    this.productClick = new EventEmitter();
  }

  ngOnInit() {
  }

  clickHandler(evt) {
    this.productClick.emit(this.product);
  }

}
