import {AfterContentInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Observable, Subscription} from 'rxjs';
import {Product} from '../../../shared/interfaces/interfaces';
import {ProductService} from '../../../core/services/product.service';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import {map} from 'rxjs/operators';
import {MatGridList} from '@angular/material';
import {Router} from '@angular/router';

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.scss']
})
export class CatalogueComponent implements OnInit, OnDestroy {

  dataProvider: Array<Product>;
  productStoreSub$: Subscription ;
  noOfColumns: number;

  @ViewChild('productGrid', {static: true}) grid: MatGridList;

  gridByBreakpoint = {
    xl: 8,
    lg: 6,
    md: 4,
    sm: 2,
    xs: 1
  };


  constructor(
    private productService: ProductService,
    private router: Router
  ) {
    // this.noOfColumns = 3;
  }


  ngOnInit() {
    this.productStoreSub$ = this.productService.get().subscribe(
      (data) => {
        this.dataProvider = data;
      }
    );
    this.noOfColumns = (window.innerWidth <= 500) ? 1 : 3;
  }

  onResize(event) {
    this.noOfColumns = (event.target.innerWidth <= 500) ? 1 : 3;
  }

  actionClicked(selectedProduct: Product) {
    console.log(selectedProduct.title);
    this.router.navigate([`home/product/${selectedProduct.id}`]);
  }

  ngOnDestroy() {
    if (this.productStoreSub$) {
      this.productStoreSub$.unsubscribe();
    }
  }

}
