This is a showcase app. just showing off different features and tricks with angular

Demo: https://shopping-cart-bd94d.firebaseapp.com/

Features
1) state managemnet using observable store (products and cart)
2) flex-layout for the UI - navbar along with side menu on small screens  
3) example of a low-coupled event dispatcher (shows the no. of items in the cart)
4) in place text editing - feedback module.  

Pushing to firebase:
firebase login --reauth
ng add @angular/fire@next
ng run shopping-cart:deploy


Things to add: 
https://www.npmjs.com/package/@herodevs/lazy-af
https://www.npmjs.com/package/ngx-loadable
github.com/willmendesneto/ngx-skeleton-loader
https://medium.com/@siddhartha.ng/skeleton-screens-with-angular-2-481c3dfc48dc
Differential loading
Web worker



References:
https://netbasal.com/keeping-it-simple-implementing-edit-in-place-in-angular-4fd92c4dfc70
https://tburleson-layouts-demos.firebaseapp.com/#/docs
https://github.com/mgechev/ngx-quicklink




